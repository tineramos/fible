//
//  HomeViewController.m
//  FiBLE
//
//  Created by Patricia Cesar on 1/9/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import "HomeViewController.h"

#import "SettingsViewController.h"
#import "DeviceListViewController.h"

@interface HomeViewController ()

@property (nonatomic, strong) DeviceListViewController *listViewController;

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)settingsButtonClicked:(id)sender
{
    SettingsViewController *settingsVC = [[SettingsViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:settingsVC animated:YES];
}

- (IBAction)goToDeviceList:(id)sender
{
    if (!self.listViewController) {
        self.listViewController = [[DeviceListViewController alloc] initWithNibName:@"DeviceListViewController" bundle:nil];
    }
    
    [self.navigationController pushViewController:self.listViewController animated:YES];
}

@end
