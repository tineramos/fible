//
//  DeviceListViewController.h
//  FiBLE
//
//  Created by Christine Ramos on 1/9/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>s

@interface DeviceListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CBPeripheralManagerDelegate>

@property (nonatomic, weak) IBOutlet UISearchBar *deviceSearchBar;
@property (nonatomic, weak) IBOutlet UITableView *deviceListTableView;

// used to define the settings (proximityUUID, major and minor) that are needed to set up a beacon as a transmitter
@property (nonatomic, strong) CLBeaconRegion *beaconRegion;

// property where the methods are contained to start it transmitting
@property (nonatomic, strong) CBPeripheralManager *peripheralManager;

// contains the peripheral data of the beacon
@property (nonatomic, strong) NSDictionary *beaconPeripheralData;

@end
