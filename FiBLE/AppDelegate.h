//
//  AppDelegate.h
//  FiBLE
//
//  Created by Patricia Cesar on 1/8/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) HomeViewController *homeVC;

@end
