//
//  Config.h
//  FiBLE
//
//  Created by Christine Ramos on 1/9/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#ifndef FiBLE_Config_h
#define FiBLE_Config_h

#define FIBLE_UUID         @"A5F477BA-F75E-4372-BCD0-D9F0317961D5"
#define FIBLE_IDENTIFIER   @"com.stratpoint.iBeacons"

#endif
