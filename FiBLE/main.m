//
//  main.m
//  FiBLE
//
//  Created by Patricia Cesar on 1/8/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
