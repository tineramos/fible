//
//  AdminDeviceListViewController.m
//  FiBLE
//
//  Created by Patricia Cesar on 1/9/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import "AdminDeviceListViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>

#define SERVICE_UUID @"D4C138AE-68A1-418C-928C-707E7A0E3C8E"

@interface AdminDeviceListViewController () <UITableViewDataSource, UITableViewDelegate, CBCentralManagerDelegate>

@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheralManager *peripheralManager;
@property (strong, nonatomic) CBPeripheral *selectedPeripheral;
@property (strong, nonatomic) NSArray *arrayOfPeripherals;

@end

@implementation AdminDeviceListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Devices";
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    [self.centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:SERVICE_UUID]] options:@{CBCentralManagerScanOptionAllowDuplicatesKey: [NSNumber numberWithBool:NO]}];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%d", indexPath.row];
    
    
    return cell;
}
//
//#pragma mark - Peripheral Manager
//
//- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
//{
//    
//}
//
#pragma mark - Central Manager 

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state == CBCentralManagerStatePoweredOn) {
        NSDictionary *options = @{CBConnectPeripheralOptionNotifyOnNotificationKey: [NSNumber numberWithBool:YES]};
        [self.centralManager connectPeripheral:self.selectedPeripheral options:options];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
