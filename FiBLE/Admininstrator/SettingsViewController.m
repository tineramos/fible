//
//  AdminAuthenticationViewController.m
//  FiBLE
//
//  Created by Patricia Cesar on 1/9/14.
//  Copyright (c) 2014 Stratpoint Technologies, Inc. All rights reserved.
//

#import "SettingsViewController.h"
#import "AdminDeviceListViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Settings";
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)manageDevicesButtonClicked:(id)sender
{
    if ([self accountIsValid]) {
        AdminDeviceListViewController *adminDeviceListVC = [[AdminDeviceListViewController alloc] initWithNibName:nil bundle:nil];
        [self.navigationController pushViewController:adminDeviceListVC animated:YES];
    }
    else {
        [UIAlertView alertViewWithTitle:@"Authentication Failed" message:@"Username or password is invalid" cancelButtonTitle:nil otherButtonTitles:@[@"Ok"] onDismiss:^(int buttonIndex) {
            
        } onCancel:^{
            
        }];
    }
    
}

- (BOOL)accountIsValid
{
    if (self.usernameTextField.text.length == 0 || self.passwordTextField.text.length == 0) {
        return NO;
    }
    return YES;
}

@end
